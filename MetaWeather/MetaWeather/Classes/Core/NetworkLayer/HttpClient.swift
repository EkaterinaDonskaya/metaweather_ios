//
//  HttpClient.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 13.03.2022.
//

import Foundation
import Alamofire

protocol HttpClientProtocol {
    func load<T: Codable>(request: NetworkRequestParams & URLRequestConvertible, completionHandler: @escaping (_ result: RequestResult<T>) -> Void)
}

final class HttpClient: HttpClientProtocol {
    
    private let sessionManager: Alamofire.Session
    private let mapper: MapperProtocol
    
    init(mapper: MapperProtocol = Mapper()) {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForResource = 45
        
        self.sessionManager = Session(configuration: configuration)
        self.mapper = mapper
    }
    
    deinit {
        sessionManager.session.getAllTasks { (task) in
            task.forEach { $0.cancel() }
        }
    }
    
    // MARK: - HttpClientProtocol
    
    func load<T: Codable>(request: NetworkRequestParams & URLRequestConvertible, completionHandler: @escaping (_ result: RequestResult<T>) -> Void) {
        sendRequest(request: request) { (response) in
            switch response {
            case let .success(data):
                let mapperResult: RequestResult<T> = self.mapper.map(data: data)
                completionHandler(mapperResult)
            case let .failure(error):
                completionHandler(.failure(error))
            }
        }
    }
    
    // MARK: - Private
    
    private func sendRequest(request: NetworkRequestParams & URLRequestConvertible, completionHandler: @escaping (_ result: RequestResult<Data>) -> Void) {
        let urlRequest = try! request.asURLRequest()
        debugPrint(urlRequest)
        sessionManager.request(urlRequest).responseData(queue: DispatchQueue.global(qos: .utility)) { [weak self] (response) in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.handle(response: response, completionHandler: completionHandler)
            }
        }
    }
    
    // MARK: - Handle Errors
    
    private func handle(response: AFDataResponse<Data>, completionHandler: @escaping (_ result: RequestResult<Data>) -> Void) {
        
        if let error = response.error {
            if error.responseCode == NSURLErrorNotConnectedToInternet {
                completionHandler(.failure(Error.noConnection))
                return
            }
            completionHandler(.failure(Error.timeout))
            return
        }
        
        guard let httpResponse = response.response else {
            completionHandler(.failure(Error.noData))
            return
        }
        
        switch httpResponse.statusCode {
        case 200...205:
            guard let data = response.data else {
                completionHandler(.failure(Error.noData))
                return
            }
            if let error = self.handleError(data: data, httpStatusCode: httpResponse.statusCode) {
                completionHandler(.failure(error))
                return
            }
            completionHandler(.success(data))
            
        case 400..<500:
            
            if httpResponse.statusCode == 401 {
                debugPrint("Unauthorized")
                completionHandler(.failure(Error.notFound))
                return
            }
            
            if let responseData = response.data {
                if let error = self.handleError(data: responseData, httpStatusCode: httpResponse.statusCode) {
                    completionHandler(.failure(error))
                    return
                }
                
                let error = Error.somethingWentWrong(httpStatusCode: httpResponse.statusCode)
                completionHandler(.failure(error))
                return
                
            } else {
                completionHandler(.failure(Error.noData))
            }

        case 500..<600:
            guard let data = response.data else {
                completionHandler(.failure(Error.internalServerError))
                return
            }
            if let error = self.handleError(data: data, httpStatusCode: httpResponse.statusCode) {
                completionHandler(.failure(error))
                return
            }
        default:
            break
        }

    }
    
    private func handleError(data: Data, httpStatusCode: Int) -> Error? {
        guard let errorJson = try? JSONSerialization.jsonObject(with: data, options: []), let json = errorJson as? [String: Any] else { return nil }
        
        guard let success = json["success"] as? Bool else { return nil }
        if success == false {
            let error = Error.serverError(json: json)
            return error
        }
        return nil
    }
}
