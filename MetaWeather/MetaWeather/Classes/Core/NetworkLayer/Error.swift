//
//  Error.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 13.03.2022.
//

import Foundation

enum Error: Swift.Error {
    
    case serverError(json: [String: Any])
    case noConnection
    case somethingWentWrong(httpStatusCode: Int)
    case timeout
    case notFound
    case noData
    case parsing
    case internalServerError
    case urlErrorCancelled
    
    var NSURLErrorCancelled: Bool {
        switch self {
        case .urlErrorCancelled:
            return true
        default:
            return false
        }
    }
    
    var errorMessage: String? {
        switch self {
        case .serverError:
            return "Something went wrong, server error"
        case .noConnection:
            return "Internet connection required"
        case .somethingWentWrong(let httpStatusCode):
            return "Something went wrong, error code: \(httpStatusCode)"
        case .timeout, .urlErrorCancelled:
            return "Connection time out"
        case .notFound:
            return "Something went wrong"
        case .noData:
            return "Data is missing"
        case .internalServerError:
            return "Internal Server Error 500"
        case .parsing:
            return "Parsing Error"
        }
    }
}
