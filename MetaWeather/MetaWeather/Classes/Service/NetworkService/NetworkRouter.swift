//
//  NetworkRouter.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 13.03.2022.
//

import Foundation
import Alamofire

enum NetworkRouter: URLRequestConvertible {
    case search(query: String)
    case showDays(woeid: Int)
}

extension NetworkRouter: NetworkRequestParams {
    
    var path: String {
        switch self {
        case .search:
            return "location/search/"
        case .showDays(let woeid):
            return "location/\(woeid)/"
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .search(let query):
            return ["query": query]
        default:
            return nil
        }
    }

    var method: HTTPMethod {
        switch self {
        case .search, .showDays: return .get
        }
    }
}
