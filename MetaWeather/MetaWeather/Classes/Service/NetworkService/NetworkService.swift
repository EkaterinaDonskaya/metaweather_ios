//
//  NetworkService.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 11.03.2022.
//

import Foundation
import Alamofire

protocol NetworkServiceProtocol {
    func searchNewCity(text: String, completionHandler: @escaping (RequestResult<[Weather]>) -> Void)
    func getDays(woeid: Int, completionHandler: @escaping (RequestResult<Weather>) -> Void)
}

final class NetworkService: NetworkServiceProtocol {
    
    static let shared = NetworkService()
    
    private let httpClient: HttpClientProtocol
    
    
    init(httpClient: HttpClientProtocol = HttpClient()) {
        self.httpClient = httpClient
    }
    
    func searchNewCity(text: String, completionHandler: @escaping (RequestResult<[Weather]>) -> Void) {
        let request = NetworkRouter.search(query: text)
        httpClient.load(request: request, completionHandler: completionHandler)
    }
    
    func getDays(woeid: Int, completionHandler: @escaping (RequestResult<Weather>) -> Void) {
        let request = NetworkRouter.showDays(woeid: woeid)
        httpClient.load(request: request, completionHandler: completionHandler)
    }
    
}
