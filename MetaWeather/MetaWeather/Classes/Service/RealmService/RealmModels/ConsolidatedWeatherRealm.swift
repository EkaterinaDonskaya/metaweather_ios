//
//  ConsolidatedWeatherRealm.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 16.03.2022.
//

import Foundation
import RealmSwift

class ConsolidatedWeatherRealm: EmbeddedObject {
    @Persisted var id: Int?
    @Persisted var weatherStateName: String?
    @Persisted var weatherStateAbbr: String?
    @Persisted var windDirectionCompass: String?
    @Persisted var created: String?
    @Persisted var applicableDate: String?
    @Persisted var minTemp: Double?
    @Persisted var maxTemp: Double?
    @Persisted var theTemp: Double?
    @Persisted var windSpeed: Double?
    @Persisted var windDirection: Double?
    @Persisted var airPressure: Double?
    @Persisted var humidity: Int?
    @Persisted var visibility: Double?
    @Persisted var predictability: Int?
    var asset: ImageAsset?
}
