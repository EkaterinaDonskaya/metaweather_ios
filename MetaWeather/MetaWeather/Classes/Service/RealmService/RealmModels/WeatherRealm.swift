//
//  WeatherRealm.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 16.03.2022.
//

import Foundation
import RealmSwift

class WeatherRealm: Object {
    @Persisted(primaryKey: true) var woeid: Int
    @Persisted var consolidatedWeather: List<ConsolidatedWeatherRealm>
    @Persisted var title: String
    
    convenience init(woeid: Int, consolidatedWeather: [ConsolidatedWeatherRealm], title: String) {
        self.init()
        self.woeid = woeid
        self.consolidatedWeather.append(objectsIn: consolidatedWeather)
        self.title = title
    }
}
