//
//  RealmStorage.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 16.03.2022.
//

import Foundation
import RealmSwift

protocol StorageProtocol {
    func save(weather: Weather) throws
    func cachedPlainObjects() -> [Weather]
    func cachedObject(byPrimaryKey key: Int) -> Weather?
    func delete(byPrimaryKey key: Int) throws
}

final class RealmStorage: StorageProtocol {
    
    static let shared = RealmStorage()
    
    init() {}
    
    func save(weather: Weather) throws {
        let realm = Realm.instance
        let realmObject = weather.toRealmObject()
        try realm.write {
            realm.add(realmObject, update: .all)
        }
        debugPrint("REALM URL", realm.configuration.fileURL as Any)
    }
    
    func cachedPlainObjects() -> [Weather] {
        let realm = Realm.instance
        let realmObjects = realm.objects(WeatherRealm.self)
        var weather: [Weather] = []
        realmObjects.forEach { item in
            weather.append(Weather(object: item))
        }
        return weather
    }
    
    func cachedObject(byPrimaryKey key: Int) -> Weather? {
        let realm = Realm.instance
        guard let realmObject = realm.object(ofType: WeatherRealm.self, forPrimaryKey: key) else { return nil }
        let translatedObject = Weather(object: realmObject)
        return translatedObject
    }
    
    func delete(byPrimaryKey key: Int) throws {
        let realm = Realm.instance
        guard let realmObject = realm.object(ofType: WeatherRealm.self, forPrimaryKey: key) else { return }
        try realm.write {
            realm.delete(realmObject)
        }
    }
}
