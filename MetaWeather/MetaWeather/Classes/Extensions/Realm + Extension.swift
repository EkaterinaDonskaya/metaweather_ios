//
//  Realm + Extension.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 16.03.2022.
//

import Foundation
import RealmSwift

extension Realm {
    static var instance: Realm {
        let realmSchemaVersion: UInt64 = 8
        return try! Realm(configuration: Realm.Configuration(schemaVersion: realmSchemaVersion, deleteRealmIfMigrationNeeded: true))
    }
}
