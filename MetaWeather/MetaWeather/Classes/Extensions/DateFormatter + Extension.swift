//
//  DateFormatter + Extension.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 12.03.2022.
//

import Foundation

extension DateFormatter {
    //"2022-03-12"
    static func convertDate(from dateString: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        if let date = dateFormatter.date(from: dateString) {
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat = "dd MMM"
            let dateString = dateFormatter.string(from: date)
            return dateString
        }
        return nil
    }
    
    //"2022-03-15T11:00:55.648590Z"
    static func convertDateAndTime(from dateString: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        if let date = dateFormatter.date(from: dateString) {
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat = "dd MMM YYYY HH:mm"
            let dateString = dateFormatter.string(from: date)
            return dateString
        }
        return nil
    }
}
