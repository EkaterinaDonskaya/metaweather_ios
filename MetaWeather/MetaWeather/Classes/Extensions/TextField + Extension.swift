//
//  TextField + Extension.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 11.03.2022.
//

import UIKit

extension UITextField {
  
    func configurePlaceholder(title: String) {
        
        attributedPlaceholder = NSAttributedString(string: title, attributes: [
            .foregroundColor: UIColor.gray,
            .font: UIFont.systemFont(ofSize: 15, weight: .semibold)])
        
        tintColor = .gray
        
    }
    
    func configurePlaceholder(title: String, font: UIFont) {
        attributedPlaceholder = NSAttributedString(string: title, attributes: [
            .foregroundColor: UIColor.gray,
            .font: font])
        
        tintColor = .gray
        
    }
    
    func setCursorLocation(_ location: Int) {
        guard let cursorLocation = position(from: beginningOfDocument, offset: location) else { return }
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.selectedTextRange = strongSelf.textRange(from: cursorLocation, to: cursorLocation)
        }
    }
    
    func setRightView(_ view: UIView, padding: CGFloat) {
        view.translatesAutoresizingMaskIntoConstraints = true
        
        let outerView = UIView()
        outerView.translatesAutoresizingMaskIntoConstraints = true
        outerView.addSubview(view)
        
        outerView.frame = CGRect(origin: .zero, size: CGSize(width: view.frame.size.width + padding, height: view.frame.size.height + padding))
        
        view.center = CGPoint(x: outerView.bounds.size.width / 2, y: outerView.bounds.size.height / 2)
        
        rightView = outerView
    }
}
