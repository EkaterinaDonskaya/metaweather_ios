//
//  Weather.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 11.03.2022.
//

import Foundation

class Weather: Codable {
    
    var woeid: Int
    var consolidatedWeather: [ConsolidatedWeather]?
    var title: String

    enum CodingKeys: String, CodingKey {
        case consolidatedWeather = "consolidated_weather"
        case title
        case woeid
    }
    
    internal init(woeid: Int, consolidatedWeather: [ConsolidatedWeather]? = nil, title: String) {
        self.woeid = woeid
        self.consolidatedWeather = consolidatedWeather
        self.title = title
    }
    
    init(object: WeatherRealm) {
        var consolidated: [ConsolidatedWeather] = []
        object.consolidatedWeather.forEach { item in
            let newItem = ConsolidatedWeather(object: item)
            consolidated.append(newItem)
        }
        self.woeid = object.woeid
        self.title = object.title
        self.consolidatedWeather = consolidated
    }
    
    func toRealmObject() -> WeatherRealm {
        var consolidated: [ConsolidatedWeatherRealm] = []
        self.consolidatedWeather?.forEach { item in
            let newItem = item.toRealmObject()
            consolidated.append(newItem)
        }
        let object = WeatherRealm(woeid: self.woeid, consolidatedWeather: consolidated, title: self.title)
        return object
    }
    
}
