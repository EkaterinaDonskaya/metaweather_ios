//
//  ConsolidatedWeather.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 11.03.2022.
//

import Foundation

class ConsolidatedWeather: Codable {
    let id: Int?
    let weatherStateName, weatherStateAbbr, windDirectionCompass, created: String?
    let applicableDate: String?
    let minTemp, maxTemp, theTemp, windSpeed: Double?
    let windDirection, airPressure: Double?
    let humidity: Int?
    let visibility: Double?
    let predictability: Int?
    var asset: ImageAsset?

    enum CodingKeys: String, CodingKey {
        case id
        case weatherStateName = "weather_state_name"
        case weatherStateAbbr = "weather_state_abbr"
        case windDirectionCompass = "wind_direction_compass"
        case created
        case applicableDate = "applicable_date"
        case minTemp = "min_temp"
        case maxTemp = "max_temp"
        case theTemp = "the_temp"
        case windSpeed = "wind_speed"
        case windDirection = "wind_direction"
        case airPressure = "air_pressure"
        case humidity, visibility, predictability
    }
    
    init(object: ConsolidatedWeatherRealm) {
        self.id = object.id
        self.weatherStateName = object.weatherStateName
        self.weatherStateAbbr = object.weatherStateAbbr
        self.windDirectionCompass = object.windDirectionCompass
        self.created = object.created
        self.applicableDate = object.applicableDate
        self.minTemp = object.minTemp
        self.maxTemp = object.maxTemp
        self.theTemp = object.theTemp
        self.windSpeed = object.windSpeed
        self.windDirection = object.windDirection
        self.airPressure = object.airPressure
        self.humidity = object.humidity
        self.visibility = object.visibility
        self.predictability = object.predictability
        self.asset = object.asset
    }
    
    func toRealmObject() -> ConsolidatedWeatherRealm {
        let object = ConsolidatedWeatherRealm()
        object.id = self.id
        object.weatherStateName = self.weatherStateName
        object.weatherStateAbbr = self.weatherStateAbbr
        object.windDirectionCompass = self.windDirectionCompass
        object.created = self.created
        object.applicableDate = self.applicableDate
        object.minTemp = self.minTemp
        object.maxTemp = self.maxTemp
        object.theTemp = self.theTemp
        object.windSpeed = self.windSpeed
        object.windDirection = self.windDirection
        object.airPressure = self.airPressure
        object.humidity = self.humidity
        object.visibility = self.visibility
        object.predictability = self.predictability
        object.asset = self.asset
        return object
    }
}
