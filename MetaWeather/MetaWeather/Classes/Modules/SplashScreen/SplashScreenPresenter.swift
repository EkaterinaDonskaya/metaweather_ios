//
//  SplashScreenPresenter.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 11.03.2022.
//

import Foundation

protocol SplashScreenPresenterProtocol: AnyObject {
    func saveCities(completionHandler: @escaping () -> Void)
}

final class SplashScreenPresenter: SplashScreenPresenterProtocol {
    
    private weak var view: SplahScreenViewControllerProtocol?
    private var router: SplashScreenRouterProtocol?
    private let storage: StorageProtocol
    
    init(view: SplahScreenViewControllerProtocol,
         router: SplashScreenRouterProtocol,
         storage: StorageProtocol = RealmStorage.shared) {
        self.view = view
        self.router = router
        self.storage = storage
    }
    
    func saveCities(completionHandler: @escaping () -> Void) {
        let cities = storage.cachedPlainObjects()
        if cities.isEmpty {
            try? storage.save(weather: Weather(woeid: 839722, consolidatedWeather: nil, title: "Sofia"))
            try? storage.save(weather: Weather(woeid: 2459115, consolidatedWeather: nil, title: "New York"))
            try? storage.save(weather: Weather(woeid: 1118370, consolidatedWeather: nil, title: "Tokio"))
        }
        completionHandler()
    }
}
