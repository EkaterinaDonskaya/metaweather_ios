//
//  SplashScreenRouter.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 11.03.2022.
//

import Foundation

protocol SplashScreenRouterProtocol: BaseWeatherRouterProtocol {
    func showCitiesList()
}

final class SplashScreenRouter: BaseWeatherRouter, SplashScreenRouterProtocol {
    func showCitiesList() {
        let controller = CitiesListViewController.controller()
        sourceVC?.navigationController?.pushViewController(controller, animated: true)
    }
    
}
