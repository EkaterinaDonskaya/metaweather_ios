//
//  SplashScreenViewController.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 11.03.2022.
//

import UIKit

protocol SplahScreenViewControllerProtocol: BaseViewProtocol {
    
}

class SplashScreenViewController: BaseViewController {
    
    private lazy var router: SplashScreenRouterProtocol = SplashScreenRouter(sourceVC: self)
    private lazy var presenter: SplashScreenPresenterProtocol = SplashScreenPresenter(view: self, router: router)
    
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        configureData()
    }
    
    private func configureData() {
        presenter.saveCities {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.activityIndicator.stopAnimating()
                self.router.showCitiesList()
            }
        }
    }
    
    private func configureUI() {
        view.backgroundColor = .white
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
    }
    

}

// MARK: - SplahScreenViewControllerProtocol
extension SplashScreenViewController: SplahScreenViewControllerProtocol {}
