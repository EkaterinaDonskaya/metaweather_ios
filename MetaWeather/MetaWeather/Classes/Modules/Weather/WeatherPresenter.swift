//
//  WeatherPresenter.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 15.03.2022.
//

import Foundation

protocol WeatherPresenterProtocol: AnyObject {
    func showWeather(weather: ConsolidatedWeather)
}

final class WeatherPresenter: WeatherPresenterProtocol {
    
    private weak var view: WeatherViewControllerProtocol?
    private var router: WeatherRouterProtocol?
    
    private let assets: [ImageAsset] = [Asset.s, Asset.h, Asset.lc, Asset.c, Asset.hc, Asset.hr, Asset.lr, Asset.sl, Asset.sn, Asset.t]
    
    init(view: WeatherViewControllerProtocol,
         router: WeatherRouterProtocol) {
        self.view = view
        self.router = router
    }
    
    func showWeather(weather: ConsolidatedWeather) {
        var weatherAsset = Asset.s
        if let asset = weather.asset {
            weatherAsset = asset
        } else {
            if self.assets.contains(where: { $0.name == weather.weatherStateAbbr } ) {
                let asset = self.assets.filter({ $0.name == weather.weatherStateAbbr }).first
                weather.asset = asset
                weatherAsset = asset ?? Asset.s
            }
        }
        
        let dateAndTime = "Updated: " + (DateFormatter.convertDateAndTime(from: weather.created ?? "") ?? "")
        let temp = Int(weather.theTemp?.rounded(.toNearestOrAwayFromZero) ?? 0)
        let maxTemp = Int(weather.maxTemp?.rounded(.toNearestOrAwayFromZero) ?? 0)
        let minTemp = Int(weather.minTemp?.rounded(.toNearestOrAwayFromZero) ?? 0)
        let windSpeed = Int(weather.windSpeed?.rounded(.toNearestOrAwayFromZero) ?? 0)
        let wind = "\(windSpeed)" + " m/h" + " " + (weather.windDirectionCompass ?? "")
        let airPressure = "\(Int(weather.airPressure ?? 0))" + " m/bar"
        let humidity = "\(weather.humidity ?? 0)" + " ％"
        let visibility = "\(Int(weather.visibility?.rounded(.toNearestOrAwayFromZero) ?? 0))" + " miles"
        let predictability = "\(weather.predictability ?? 0)" + " ％"
        
        view?.updateUI(dateAndTime: dateAndTime, image: weatherAsset, description: weather.weatherStateName ?? "", temp: "\(temp)", maxTemp: "\(maxTemp)", minTemp: "\(minTemp)", windSpeed: wind, airPressure: airPressure, humidity: humidity, visibility: visibility, predictability: predictability)
    }
    
}
