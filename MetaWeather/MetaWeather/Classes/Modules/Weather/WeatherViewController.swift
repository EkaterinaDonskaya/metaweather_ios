//
//  WeatherViewController.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 15.03.2022.
//

import UIKit

protocol WeatherViewControllerProtocol: BaseViewProtocol {
    func updateUI(dateAndTime: String, image: ImageAsset, description: String, temp: String, maxTemp: String, minTemp: String, windSpeed: String, airPressure: String, humidity: String, visibility: String, predictability: String)
}

class WeatherViewController: BaseViewController {
    
    private lazy var router: WeatherRouterProtocol = WeatherRouter(sourceVC: self)
    private lazy var presenter: WeatherPresenterProtocol = WeatherPresenter(view: self, router: router)
    private var weather: ConsolidatedWeather!

    // MARK: -IBOutlet
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var weatherView: UIView! {
        didSet {
            weatherView.backgroundColor = .white
            weatherView.addShadow(color: UIColor(red: 0.263, green: 0.31, blue: 0.133, alpha: 0.25), offSet: CGSize(width: 0.0, height: 0.0), shadowRadius: 4.0, cornerRadius: 10.0)
        }
    }
    
    @IBOutlet weak var dateAndTimeLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var weatherDescription: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var airPressureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var visibilityLabel: UILabel!
    @IBOutlet weak var predictabilityLabel: UILabel!
    
    // MARK: -LifeCycle
    
    static func controller(weather: ConsolidatedWeather) -> WeatherViewController {
        let storyBoard = UIStoryboard(name: "Weather", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "Weather") as! WeatherViewController
        viewController.weather = weather
        return viewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter.showWeather(weather: weather)
    }
    
    // MARK: - Actions
    @IBAction func backButtonAction(_ sender: Any) {
        router.dismiss(animated: true)
    }
    
}

// MARK: - WeatherViewControllerProtocol
extension WeatherViewController: WeatherViewControllerProtocol {
    func updateUI(dateAndTime: String, image: ImageAsset, description: String, temp: String, maxTemp: String, minTemp: String, windSpeed: String, airPressure: String, humidity: String, visibility: String, predictability: String) {
        dateAndTimeLabel.text = dateAndTime
        weatherImageView.image = image.image
        weatherDescription.text = description
        tempLabel.text = temp + "℃"
        maxTempLabel.text = "max: " + maxTemp + "℃"
        minTempLabel.text = "min: " + minTemp + "℃"
        windSpeedLabel.text = "Wind: " + windSpeed
        airPressureLabel.text = "Air pressure: " + airPressure
        humidityLabel.text = "Humidity: " + humidity
        visibilityLabel.text = "Visibility: " + visibility
        predictabilityLabel.text = "Predictability: " + predictability
    }
}
