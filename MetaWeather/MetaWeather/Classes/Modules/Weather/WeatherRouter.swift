//
//  WeatherRouter.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 15.03.2022.
//

import Foundation

protocol WeatherRouterProtocol: BaseWeatherRouterProtocol {
    
}

final class WeatherRouter: BaseWeatherRouter, WeatherRouterProtocol {
    
}
