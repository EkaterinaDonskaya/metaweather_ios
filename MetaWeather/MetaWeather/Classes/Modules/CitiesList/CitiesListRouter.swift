//
//  CitiesListRouter.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 11.03.2022.
//

import Foundation

protocol CitiesListRouterProtocol: BaseWeatherRouterProtocol {
    func showDaysList(woeid: Int)
}

final class CitiesListRouter: BaseWeatherRouter, CitiesListRouterProtocol {
    func showDaysList(woeid: Int) {
        let controller = DaysListViewController.controller(woeid: woeid)
        sourceVC?.navigationController?.pushViewController(controller, animated: true)
    }
}
