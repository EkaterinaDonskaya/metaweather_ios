//
//  CitiesListPresenter.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 11.03.2022.
//

import Foundation

protocol CitiesListPresenterProtocol: AnyObject {
    var cities: [Weather] { get }
    func loadCities()
    func searchBarTextChanged(searchText: String)
    func removeCity(woeid: Int)
    func ifSaved(woeid: Int) -> Bool
}

final class CitiesListPresenter: CitiesListPresenterProtocol {
    
    private weak var view: CitiesListViewControllerProtocol?
    private var router: CitiesListRouterProtocol?
    private var networkService: NetworkServiceProtocol?
    private let storage: StorageProtocol
    
    var cities: [Weather] = []
    
    init(view: CitiesListViewControllerProtocol,
         router: CitiesListRouterProtocol,
         networkService: NetworkServiceProtocol = NetworkService.shared,
         storage: StorageProtocol = RealmStorage.shared) {
        self.view = view
        self.router = router
        self.networkService = networkService
        self.storage = storage
    }
    
    func loadCities() {
        view?.showLoader()
        cities = storage.cachedPlainObjects()
        view?.configureTableView()
        view?.hideLoader()
    }
    
    func searchBarTextChanged(searchText: String) {
        if searchText == "" {
            loadCities()
            return
        }
        
        view?.showLoader()
        networkService?.searchNewCity(text: searchText, completionHandler: { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let cities):
                self.cities = cities
                self.view?.configureTableView()
                self.view?.hideLoader()
            case .failure(let error):
                self.view?.hideLoader()
                if let message = error.errorMessage {
                    self.view?.showAlert(titleAlert: "Error", messageText: message, titleCancelAction: "OK")
                } else {
                    debugPrint(error.localizedDescription)
                }
            }
        })
    }
    
    func removeCity(woeid: Int) {
        view?.showLoader()
        if cities.contains(where: { $0.woeid == woeid }) {
            cities.removeAll(where: { $0.woeid == woeid })
            try? storage.delete(byPrimaryKey: woeid)
            view?.configureTableView()
        }
        view?.hideLoader()
    }
    
    func ifSaved(woeid: Int) -> Bool {
        let savedCities = storage.cachedPlainObjects()
        return savedCities.contains(where: { $0.woeid == woeid })
    }
}
