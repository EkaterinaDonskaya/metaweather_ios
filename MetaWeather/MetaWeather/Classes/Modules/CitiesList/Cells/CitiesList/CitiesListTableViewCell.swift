//
//  CitiesListTableViewCell.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 11.03.2022.
//

import UIKit
import TableKit

struct CitiesListTableViewCellActions {
    static let removeCity = "RemoveCity"
}

class CitiesListTableViewCell: UITableViewCell, ConfigurableCell {
    
    static var defaultHeight: CGFloat? {
        return 50.0
    }
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var removeButton: UIButton! {
        didSet {
            removeButton.setImage(Asset.removeCity.image, for: .normal)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        selectionStyle = .none
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.contentView.backgroundColor = .white
        cellView.layer.cornerRadius = 5
        cellView.clipsToBounds = true
        cellView.backgroundColor = .white
    }
    
    
    @IBAction func removeDuttonAction(_ sender: Any) {
        TableCellAction(key: CitiesListTableViewCellActions.removeCity, sender: self).invoke()
    }
    
    // MARK: - ConfigurableCell
    
    func configure(with item: Weather) {
        nameLabel.text = item.title
    }
    
}
