//
//  CitiesListViewController.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 11.03.2022.
//

import UIKit
import TableKit

protocol CitiesListViewControllerProtocol: BaseViewProtocol {
    func configureTableView()
    func showAlert(titleAlert: String, messageText: String, titleCancelAction: String)
}

class CitiesListViewController: BaseViewController {
    
    private lazy var router: CitiesListRouterProtocol = CitiesListRouter(sourceVC: self)
    private lazy var presenter: CitiesListPresenterProtocol = CitiesListPresenter(view: self, router: router)
    private lazy var tableDirector: TableDirector = TableDirector(tableView: citiesTableView)
    
    // MARK: -IBOutlet
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var citiesTableView: UITableView!
    
    // MARK: -LifeCycle
    
    static func controller() -> CitiesListViewController {
        let storyBoard = UIStoryboard(name: "CitiesList", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CitiesList") as! CitiesListViewController
        return viewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        presenter.loadCities()
    }
    
    private func configureUI() {
        view.backgroundColor = .white
        
        searchBar.delegate = self
        searchBar.showsCancelButton = false
        searchBar.searchTextField.configurePlaceholder(title: "Search")
        
        if let textFieldSearchBar = searchBar.value(forKey: "searchField") as? UITextField, let clearButton = textFieldSearchBar.value(forKey: "_clearButton")as? UIButton {
            
            clearButton.addTarget(self, action: #selector(clearSearchBar), for: .touchUpInside)
            clearButton.setImage(Asset.redClose.image, for: .normal)
            textFieldSearchBar.clearButtonMode = .always
            
            if let glassIconView = textFieldSearchBar.leftView as? UIImageView {
                glassIconView.image = glassIconView.image?.withRenderingMode(.alwaysTemplate)
                glassIconView.tintColor = UIColor.gray
            }
        }
    }
    
    
    // MARK: - Actions
    
    @objc func clearSearchBar() {
        searchBar.text = ""
        searchBar.resignFirstResponder()
        presenter.loadCities()
    }

}

// MARK: - CitiesListViewControllerProtocol
extension CitiesListViewController: CitiesListViewControllerProtocol {
    func configureTableView() {
        
        tableDirector.clear()
        let items = presenter.cities
        var sections = [TableSection]()
        
        let section = TableSection()
        
        items.forEach { item in
            
            
            let row = TableRow<CitiesListTableViewCell>(item: item)
                .on(.willDisplay) { [weak self] options in
                    guard let self = self else { return }
                    if self.presenter.ifSaved(woeid: item.woeid) {
                        options.cell?.removeButton.isHidden = false
                    } else {
                        options.cell?.removeButton.isHidden = true
                    }
                }
                .on(.click) { [weak self] options in
                    guard let self = self else { return }
                    self.router.showDaysList(woeid: item.woeid)
                }
                .on(.custom(CitiesListTableViewCellActions.removeCity)) { [weak self] options in
                    guard let self = self else { return }
                    self.presenter.removeCity(woeid: item.woeid)
                }
            
            section.append(row: row)
        }
        section.headerHeight = 0
        section.footerHeight = 0
        sections.append(section)
        
        tableDirector.append(sections: sections)
        tableDirector.reload()
    }
    
    func showAlert(titleAlert: String, messageText: String, titleCancelAction: String) {
        let cancelAction = UIAlertAction(title: titleCancelAction, style: .cancel, handler: { (actiion) in
            
        })
        let alert = UIAlertController(title: titleAlert, message: messageText, preferredStyle: .alert)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
}


// MARK: - UISearchBarDelegate
extension CitiesListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter.searchBarTextChanged(searchText: searchText)
    }
}
