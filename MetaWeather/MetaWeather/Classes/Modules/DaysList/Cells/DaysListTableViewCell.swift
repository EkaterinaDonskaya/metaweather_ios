//
//  DaysListTableViewCell.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 12.03.2022.
//

import UIKit
import TableKit

class DaysListTableViewCell: UITableViewCell, ConfigurableCell {
    
    static var defaultHeight: CGFloat? {
        return 50.0
    }
    @IBOutlet weak var cellView: UIView!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    
    
    @IBOutlet weak var weatherImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        selectionStyle = .none
    }
    
    // MARK: - ConfigurableCell
    
    func configure(with item: ConsolidatedWeather) {
        let date = DateFormatter.convertDate(from: item.applicableDate ?? "")
        let tempInt = Int(item.theTemp?.rounded(.toNearestOrAwayFromZero) ?? 0)
        dateLabel.text = date
        tempLabel.text = "\(tempInt)"
        weatherImageView.image = item.asset?.image
    }
    
}
