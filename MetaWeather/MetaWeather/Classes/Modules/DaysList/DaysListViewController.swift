//
//  DaysListViewController.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 12.03.2022.
//

import UIKit
import TableKit

protocol DaysListViewControllerProtocol: BaseViewProtocol {
    func configureTableView()
    func showAlert(titleAlert: String, messageText: String, titleCancelAction: String)
}

class DaysListViewController: BaseViewController {
    
    private lazy var router: DaysListRouterProtocol = DaysListRouter(sourceVC: self)
    private lazy var presenter: DaysListPresenterProtocol = DaysListPresenter(view: self, router: router)
    private lazy var tableDirector: TableDirector = TableDirector(tableView: daysTableView)
    
    private var woeid: Int!
    
    // MARK: -IBOutlet
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var daysTableView: UITableView!
    
    // MARK: -LifeCycle
    
    static func controller(woeid: Int) -> DaysListViewController {
        let storyBoard = UIStoryboard(name: "DaysList", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DaysList") as! DaysListViewController
        viewController.woeid = woeid
        return viewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        presenter.loadDays(woeid: woeid)
    }
    
    // MARK: - Actions
    
    @IBAction func backButtonAction(_ sender: Any) {
        router.dismiss(animated: true)
    }
    


}

// MARK: - DaysListViewControllerProtocol
extension DaysListViewController: DaysListViewControllerProtocol {
    func configureTableView() {
        
        tableDirector.clear()
        let items = presenter.days
        var sections = [TableSection]()
        
        let section = TableSection()
        
        items.forEach { item in
            
            
            let row = TableRow<DaysListTableViewCell>(item: item)
                .on(.click) { [weak self] options in
                    guard let self = self else { return }
                    self.router.showWeather(weather: item)
                }
            
            section.append(row: row)
        }
        section.headerHeight = 0
        section.footerHeight = 0
        sections.append(section)
        
        tableDirector.append(sections: sections)
        tableDirector.reload()
        
    }
    
    func showAlert(titleAlert: String, messageText: String, titleCancelAction: String) {
        let cancelAction = UIAlertAction(title: titleCancelAction, style: .cancel, handler: { (actiion) in
            
        })
        let alert = UIAlertController(title: titleAlert, message: messageText, preferredStyle: .alert)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
}
