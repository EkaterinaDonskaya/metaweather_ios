//
//  DaysListRouter.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 12.03.2022.
//

import Foundation

protocol DaysListRouterProtocol: BaseWeatherRouterProtocol {
    func showWeather(weather: ConsolidatedWeather)
}

final class DaysListRouter: BaseWeatherRouter, DaysListRouterProtocol {
    func showWeather(weather: ConsolidatedWeather) {
        let controller = WeatherViewController.controller(weather: weather)
        sourceVC?.navigationController?.pushViewController(controller, animated: true)
    }
}
