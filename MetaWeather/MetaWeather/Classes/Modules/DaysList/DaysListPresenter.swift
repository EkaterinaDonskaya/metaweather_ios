//
//  DaysListPresenter.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 12.03.2022.
//

import Foundation

protocol DaysListPresenterProtocol: AnyObject {
    var days: [ConsolidatedWeather] { get }
    func loadDays(woeid: Int)
}

final class DaysListPresenter: DaysListPresenterProtocol {
    
    private weak var view: DaysListViewControllerProtocol?
    private var router: DaysListRouterProtocol?
    private var networkService: NetworkServiceProtocol?
    private let storage: StorageProtocol
    
    var days: [ConsolidatedWeather] = []
    private let assets: [ImageAsset] = [Asset.s, Asset.h, Asset.lc, Asset.c, Asset.hc, Asset.hr, Asset.lr, Asset.sl, Asset.sn, Asset.t]
    
    init(view: DaysListViewControllerProtocol,
         router: DaysListRouterProtocol,
         networkService: NetworkServiceProtocol = NetworkService.shared,
         storage: StorageProtocol = RealmStorage.shared) {
        self.view = view
        self.router = router
        self.networkService = networkService
        self.storage = storage
    }
    
    func loadDays(woeid: Int) {
        view?.showLoader()
        networkService?.getDays(woeid: woeid, completionHandler: { result in
            switch result {
            case .success(let weather):
                try? self.storage.save(weather: weather)
                self.configureTableView(weather: weather)
            case .failure(let error):
                self.view?.hideLoader()
                if let message = error.errorMessage {
                    self.view?.showAlert(titleAlert: "Error", messageText: message, titleCancelAction: "OK")
                } else {
                    debugPrint(error.localizedDescription)
                }
                if let realmWeather = self.storage.cachedObject(byPrimaryKey: woeid) {
                    self.configureTableView(weather: realmWeather)
                }
            }
        })
    }
    
    private func configureTableView(weather: Weather) {
        guard let days = weather.consolidatedWeather else {
            view?.hideLoader()
            return }
        
        days.forEach { day in
            if self.assets.contains(where: { $0.name == day.weatherStateAbbr } ) {
                let asset = self.assets.filter({ $0.name == day.weatherStateAbbr }).first
                day.asset = asset
            }
        }
        
        self.days = days
        self.view?.configureTableView()
        self.view?.hideLoader()
    }
}
