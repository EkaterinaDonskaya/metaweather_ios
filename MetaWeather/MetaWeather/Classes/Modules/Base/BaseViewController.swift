//
//  BaseViewController.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 11.03.2022.
//

import UIKit
import IHProgressHUD

protocol BaseViewProtocol: AnyObject {
    func showLoader()
    func hideLoader()
}

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
       
    }

}

extension BaseViewController: BaseViewProtocol {
    func showLoader() {
        IHProgressHUD.set(defaultMaskType: .black)
        IHProgressHUD.show()
    }
    
    func hideLoader() {
        IHProgressHUD.dismiss()
    }
}

