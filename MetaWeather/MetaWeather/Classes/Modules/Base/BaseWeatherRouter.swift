//
//  BaseWeatherRouter.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 11.03.2022.
//

import Foundation

protocol BaseWeatherRouterProtocol: AnyObject {
    func dismiss(animated: Bool)
}

class BaseWeatherRouter: BaseRouter, BaseWeatherRouterProtocol {
    func dismiss(animated: Bool) {
        sourceVC?.navigationController?.popViewController(animated: animated)
    }
}
