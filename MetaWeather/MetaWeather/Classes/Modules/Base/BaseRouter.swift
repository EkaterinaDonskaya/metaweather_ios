//
//  BaseRouter.swift
//  MetaWeather
//
//  Created by Ekaterina Donskaya on 11.03.2022.
//

import UIKit

class BaseRouter {
    weak var sourceVC: UIViewController?
    
    init(sourceVC: UIViewController) {
        self.sourceVC = sourceVC
    }
}
